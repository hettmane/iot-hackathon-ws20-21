// 1. Run npm install node-fetch
// 2. Import an HTTP client
const fetch = require("node-fetch");
// 3. Declare spreadsheet and values to append
const spreadsheetId = '14iejpXmGwGT6Oe5P9nv2JPI8eoIcvtk09c_JJMQrh2o'
// 4. Send data with a POST request
const baseUrl = "https://pushtogsheet.herokuapp.com";
const query = `valueInputOption=RAW&pizzly_pkey=pope8Qy8qfYyppnHRMgLMpQ8MuEUKDGeyhfGCj`;
const url = new URL(`/proxy/google-sheets/${spreadsheetId}/values/A1:append?${query}`, baseUrl);

const bodyParser = require('body-parser');
var express = require('express');
var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
// set the view engine to ejs
app.set('view engine', 'ejs');


// homepage
app.get('/', function (req, res) {
    res.render('homepage');
});

//send question to google sheet and trigger hue lamp
app.post("/yellow", function (req, res) {
    //res.render('homepage', { name: req.body.name });
    var question = req.body.frage
    var data = [[question]]

    fetch(url.href, {
        method: "POST",
        body: JSON.stringify({ values: data }),
        headers: { 'Pizzly-Auth-Id': '840e1630-5fe4-11eb-b4b8-f15d47aeecdf' }
    })
        .then((res) => res.text())
        .then(console.log)
        .catch(console.error);

    res.redirect("https://maker.ifttt.com/trigger/Turn_Lamp_On_Yellow/with/key/oAbQVitFzrNwY9tRV1Nud")
});

app.post("/green", function (req, res) {
    res.redirect("https://maker.ifttt.com/trigger/Turn_Light_On_Green/with/key/eCHhGQY4TuQMqzMofdIqlAPqbGNgDXzai6NGOtPZ29-")
});

app.post("/red", function (req, res) {
    res.redirect("https://maker.ifttt.com/trigger/Turn_Light_On_Red/with/key/eCHhGQY4TuQMqzMofdIqlAPqbGNgDXzai6NGOtPZ29-")
});

app.post("/blink", function (req, res) {
    res.redirect("https://maker.ifttt.com/trigger/Turn_Light_On_Blink/with/key/eCHhGQY4TuQMqzMofdIqlAPqbGNgDXzai6NGOtPZ29-")
});

app.listen(8080);
console.log('Server running at port 8080');
